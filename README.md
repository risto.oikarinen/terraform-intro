# terraform-intro

Simple project for terraform demo.

Creates:

- VPC
- EC2 virtual machine with security group

See simple terraform config validator in .gitlab-ci.yml.
