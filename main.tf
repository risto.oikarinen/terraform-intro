terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}

locals {
  environment = "terraform-intro"
}

# Third-party VPC module that helps with creating VPCs
module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.78.0"

  name = local.environment
  cidr = "10.0.0.0/16"

  azs             = ["${var.aws_region}a"]
  private_subnets = []
  public_subnets  = ["10.0.101.0/24"]

  enable_nat_gateway = false

  tags = {
    Environment = local.environment
  }
}
